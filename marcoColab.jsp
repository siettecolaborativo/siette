<%@ page import="siette.*, siette.colab.*" autoFlush="true"  errorPage="/excepcion.jsp" %>

<%
   Traduccion               trad;
   AlumnoSIETTEColaborativo alumno;
//   String       maquina = java.net.InetAddress.getLocalHost().getCanonicalHostName();
   String       maquina = request.getServerName();
   int         puerto  = request.getServerPort();


   String div = (String) session.getAttribute("colab.div");
   int appletWidth, appletHeight;
   if (div!=null && div.equals("H")) {
   		appletWidth = 1020;
   		appletHeight = 180;
   } else {
   		appletWidth = 300;
   		appletHeight = 550;
   }

%>

<%
  trad  = (Traduccion)session.getAttribute ("traduccion");
  if (trad == null)
  {
%>
      <jsp:forward page="<%= SIETTE.encodeURL(request, response, \"/error.jsp\")%>" >
              <jsp:param name="codigo" value="5" />
              <jsp:param name="retorno" value="<%= SIETTE.URL_REGISTRO %>" />
              <jsp:param name="debug" value="MARCOCOLABORATIVO.JSP: Error de sesion: trad es null" />
      </jsp:forward>
<%
  }

  alumno = (AlumnoSIETTEColaborativo)session.getAttribute ("alumno");
  if (alumno == null)
  {
%>
      <jsp:forward page="<%= SIETTE.encodeURL(request, response, \"/error.jsp\")%>" >
              <jsp:param name="codigo" value="5" />
              <jsp:param name="retorno" value="<%= SIETTE.URL_REGISTRO %>" />
              <jsp:param name="debug" value="MARCOCOLABORATIVO.JSP: <%=trad.get(1220)%>: alumno <%=trad.get(1221)%>" />
      </jsp:forward>
<%
  }
  String strGrupo = "IDGRUPO#"+alumno.getIdgrupo();

%>

<!-- <%= alumno.getIdgrupo() %>  -->
<html>
<head>
   <link rel="shortcut icon" href="/siette/images/favicon.ico" type="image/x-icon" />
	<title><%= trad.get(1208) %></title>
<!---------------------------------------------------------------------------->
<!-- JAVASCRIPT -------------------------------------------------------------->
<!---------------------------------------------------------------------------->

        <script type="text/javascript" src="http://crop.workkola.com/chat/js/jquery.2.1.4.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/command.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/message.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/JSON.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/constantes.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/TodasRespuestas.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/pila.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/listadoUsuarios.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/manejoChat.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/serializado.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/simuladorAjax.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/InOut.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/main.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/funcionesTest.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/outputlog.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/volteadoChat.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/cola.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/js/Ajax.js"></script>
        <script type="text/javascript" src="http://crop.workkola.com/chat/bootstrap/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="htto://crop.workkola.com/chat/js/estilo.css" />
        <link type="text/css" rel="stylesheet" href="http://crop.workkola.com/chat/bootstrap/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="http://crop.workkola.com/chat/bootstrap/css/bootstrap-theme.min.css" />

<script LANGUAGE="JavaScript">
///////////////////////////////////////////////////////////////////////////////
function ConsultarRespuestas(idalumno) {
  parent.dcha.location = "<%= SIETTE.encodeURL(request, response, SIETTE.URL_SIETTE + "colab/ConsultarRespuesta") %>?idalumno=" + idalumno
}
///////////////////////////////////////////////////////////////////////////////
function ConsultarTodasRespuestas(idalumno) {
  parent.dcha.location = "<%= SIETTE.encodeURL(request, response, SIETTE.URL_SIETTE + "colab/ConsultarTodasRespuestas") %>?idalumno=" + idalumno
}
///////////////////////////////////////////////////////////////////////////////
</script>


</head>

<body onload='inicializacion()'>
        <table id="chatH">
            <tr>
                <td>
                    <h1 id="tituloChat"></h1>
                    <div class="estado" id="estado">
                        <h1 id="EstadoUsuarios" class="titulo"><img src="img/status_img.png" /></h1>
                        <div class="usuario" id="001">
                            <ul id="listaUsuarios" class="listaUser">
                            </ul>
                        </div>
                    </div>
                    <div class="todasRes"><input id="envio" name="todasRespuestas" class="envio" type="button" onclick='JavaScript:verTodasLasRespuestas()'></div>
                </td>
                <td>
                    <div>
                        <h1 id="tituloChatUser" class="titulo2"><img src="img/posts_img.png" /></h1>
                        <form method="post" name="f1">
                            <TextArea name="result" class="chat" id="chat"></TextArea>
                        </form>
                    </div>
                </td>
                <td>
                    <div>
                        <div id="004" class="usuarios listado">
                        </div>
                        <input type="text" id="escritura" name="escritura" class="escritura"/>
                    </div>
                </td>
            </tr>
        </table>
        <!--nuevos parametros init chat-->
        <param name="urlServlet" value="http://<%= maquina  + ":" + puerto %>/siette/colab/ServidorColaborativo"/>
        <param name="term26" value="todos"/>
        <param name="term27" value="Chat de usuarios"/>
        <param name="term28" value="Escribe aqui tu mensaje"/>


        <param name="div" value="<%= div %>"/>
        <param name="debuglevel" value="<%= SIETTE.DEBUG_LEVEL %>"/>
        <param name="chatStyle" value="flat"/>
        <param name="userid" value="<%= alumno.getIdalumno() %>"/>
        <param name="username" value="<%= alumno.getNombreUsuario() %>"/>
        <param name="session" value="<%= "IDGRUPO#"+alumno.getIdgrupo() %>"/>
        <param name="URL" value="http://<%= maquina + ":" + puerto %>/siette/colab/ServidorColaborativo"/>
        <param name="jsConsultarRespuestas" value="ConsultarRespuestas"/>
        <param name="jsConsultarTodasRespuestas" value="ConsultarTodasRespuestas"/>

        <param name="term0" value="<%= trad.get(543) %>"/>
        <param name="term1" value="<%= trad.get(544) %>"/>
        <param name="term2" value="<%= trad.get(545) %>"/>
        <param name="term3" value="<%= trad.get(546) %>"/>
        <param name="term4" value="<%= trad.get(547) %>"/>
        <param name="term5" value="<%= trad.get(548) %>"/>
        <param name="term6" value="<%= trad.get(549) %>"/>
        <param name="term7" value="<%= trad.get(550) %>"/>
        <param name="term8" value="<%= trad.get(551) %>"/>
        <param name="term9" value="<%= trad.get(552) %>"/>
        <param name="term10" value="<%= trad.get(553) %>"/>
        <param name="term11" value="<%= trad.get(554) %>"/>
        <param name="term12" value="<%= trad.get(555) %>"/>
        <param name="term13" value="<%= trad.get(556) %>"/>
        <param name="term14" value="<%= trad.get(557) %>"/>
        <param name="term15" value="<%= trad.get(558) %>"/>
        <param name="term16" value="<%= trad.get(559) %>"/>
        <param name="term17" value="<%= trad.get(560) %>"/>
        <param name="term18" value="<%= trad.get(561) %>"/>
        <param name="term19" value="<%= trad.get(562) %>"/>
        <param name="term20" value="<%= trad.get(563) %>"/>
        <param name="term21" value="<%= trad.get(564) %>"/>
        <param name="term22" value="<%= trad.get(565) %>"/>
        <param name="term23" value="<%= trad.get(587) %>"/>
        <param name="term24" value="<%= trad.get(593) %>"/>
        <param name="term25" value="<%= trad.get(850) %>"/>
</body>

<!-- 
<body bgcolor="#d0d0d0" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" bottommargin="0" rightmargin="0">

<applet MAYSCRIPT archive="/siette/lib/colab.jar,/siette/lib/json-simple-1.1.1.jar"
	code="siette.colab.jgroups.AppletChatUser"
	codebase="/siette/lib"
	name="chatuser"
	width="<%= appletWidth %>"
	height="<%= appletHeight %>"
>
	<param name="div" value="<%= div %>"/>
	<param name="debuglevel" value="<%= SIETTE.DEBUG_LEVEL %>"/>
	<param name="chatStyle" value="flat"/>
	<param name="userid" value="<%= alumno.getIdalumno() %>"/>
	<param name="username" value="<%= alumno.getNombreUsuario() %>"/>
	<param name="session" value="<%= "IDGRUPO#"+alumno.getIdgrupo() %>"/>
	<param name="URL" value="http://<%= maquina + ":" + puerto %>/siette/colab/ServidorColaborativo"/>
	<param name="jsConsultarRespuestas" value="ConsultarRespuestas"/>
	<param name="jsConsultarTodasRespuestas" value="ConsultarTodasRespuestas"/>

	<param name="term0" value="<%= trad.get(543) %>"/>
	<param name="term1" value="<%= trad.get(544) %>"/>
	<param name="term2" value="<%= trad.get(545) %>"/>
	<param name="term3" value="<%= trad.get(546) %>"/>
	<param name="term4" value="<%= trad.get(547) %>"/>
	<param name="term5" value="<%= trad.get(548) %>"/>
	<param name="term6" value="<%= trad.get(549) %>"/>
	<param name="term7" value="<%= trad.get(550) %>"/>
	<param name="term8" value="<%= trad.get(551) %>"/>
	<param name="term9" value="<%= trad.get(552) %>"/>
	<param name="term10" value="<%= trad.get(553) %>"/>
	<param name="term11" value="<%= trad.get(554) %>"/>
	<param name="term12" value="<%= trad.get(555) %>"/>
	<param name="term13" value="<%= trad.get(556) %>"/>
	<param name="term14" value="<%= trad.get(557) %>"/>
	<param name="term15" value="<%= trad.get(558) %>"/>
	<param name="term16" value="<%= trad.get(559) %>"/>
	<param name="term17" value="<%= trad.get(560) %>"/>
	<param name="term18" value="<%= trad.get(561) %>"/>
	<param name="term19" value="<%= trad.get(562) %>"/>
	<param name="term20" value="<%= trad.get(563) %>"/>
	<param name="term21" value="<%= trad.get(564) %>"/>
	<param name="term22" value="<%= trad.get(565) %>"/>
	<param name="term23" value="<%= trad.get(587) %>"/>
	<param name="term24" value="<%= trad.get(593) %>"/>
	<param name="term25" value="<%= trad.get(850) %>"/>
	
	<param name="term0" value="Estado de los usuarios"/>
	<param name="term1" value="resp. individual"/>
	<param name="term2" value="discusi&oacute;n"/>
	<param name="term3" value="resp. grupo"/>
	<param name="term4" value="finalizado"/>
	<param name="term5" value="Chat"/>
	<param name="term6" value="Respuestas"/>
	<param name="term7" value="Mensajes de los usuarios del chat"/>
	<param name="term8" value="Mensajes"/>
	<param name="term9" value="Usuarios conectados a la asignatura"/>
	<param name="term10" value="Lea aqu&iacute; el contenido del mensaje seleccionado."/>
	<param name="term11" value="Asunto de los mensajes a los que se quiere responder"/>
	<param name="term12" value="Responder a"/>
	<param name="term13" value="Escriba aqu&iacute; su mensaje. Pulse <Ctrl>+<Enter> para enviarlo."/>
	<param name="term14" value="Comentario"/>
	<param name="term15" value="Etiquetar mensaje como"/>
	<param name="term16" value="Pregunta"/>
	<param name="term17" value="Respuesta"/>
	<param name="term18" value="Fundamentación"/>
	<param name="term19" value="Enviar"/>
	<param name="term20" value="Seleccione un usuario para ver su respuesta."/>
	<param name="term21" value="de"/>
	<param name="term22" value="test finalizado"/>
	<param name="term23" value="Datos de la asignatura"/>
	
</applet>

</body>
 -->



</html>

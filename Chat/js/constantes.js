var NULL = 0;
/* Comandos de la clase ServerConnection. */
/** Especifica un comando de <b>abrir nueva sesión</b>. */
var OPEN_SESSION = 1;
/** Especifica un comando de <b>cerrar sesión existente</b>. */
var CLOSE_SESSION = 2;
/** Especifica un comando de <b>obtener sesión existente</b>. */
var GET_SESSION = 3;
/* Comandos de la clase Session. */
/** Especifica un comando de <b>annadir cliente a sesión</b>. */
var ADD_CLIENT = 4;
/** Especifica un comando de <b>eliminar cliente de sesión</b>. */
var REMOVE_CLIENT = 5;
/** Especifica un comando de <b>crear un nuevo canal en una sesión</b>. */
var CREATE_CHANNEL = 6;
/** Especifica un comando de <b>eliminar un canal existente de una sesión</b>. */
var REMOVE_CHANNEL = 7;
/** Especifica un comando de <b>obtener un canal existente de una sesión</b>. */
var GET_CHANNEL = 8;
/** Especifica un comando de <b>enlazar un cliente con un observador y un canal</b>. */
var JOIN = 9;
/** Especifica un comando de <b>desenlazar un cliente de un observador y un canal</b>. */
var LEAVE = 10;

/* Comandos de la clase Channel. */

/** Especifica un comando de <b>enviar un mensaje</b>. */
var SEND = 11;

/* Comandos de la clase Consumer. */

/** Especifica un comando de <b>recibir mensajes pendientes del servidor</b>. */
var RECEIVE = 12;

/*
 * Comando del servidor: avisa a los usuarios de una sesión cuando uno de ellos
 * ha perdido la conexión.
 */

/** especifica un comando de <b>desconexión involuntario de un cliente</b>. */
var CLIENT_DISCONNECTED = 13;

/* Comandos de confirmación. */

/** Especifica un comando de <b>reconocimiento</b>. */
var ACK = 14;
/** Especifica un comando de <b>no reconocimiento</b>. */
var NACK = 15;

/** Comandos para pedir la lista de usuarios registrados en un canal/sesion */
var CLIENTLIST = 200;

var ITEM_NULL=-1;
var STEP_NULL=-1;
var INIT = 100;
var END =  199;
var ADD_USER= 1001;
var REMOVE_USER = 1002;
var WAIT_RESPONSES= 1003;
var UPDATE_USER_STATUS= 1004;
var ADD_RESPONSE= 1005;
var END_TEST= 1006;
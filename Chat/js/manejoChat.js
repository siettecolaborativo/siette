function respuesta() {
    var escri = $('#escritura')[0];
//    var destinatario = form.destinatario.value;
    var texto = escri.value;
    escri.value = "";
    // var ind = document.getElementById("listUser").selectedIndex;
    var ind = 0;
    if (texto.length > 0) {
        if (ind != 0){
            destinatario=listaUsuarios[ind].id;
            responderA(texto, destinatario);
        }else {
            responder(texto);
        }
    } else {
        escri.focus();
        muestra('Por favor rellene el campo de mensaje');
    }
}

function annadeRespuesta(usuario, texto,aling,id,id_usuario) {
    if (!repetido(id, usuario, id_usuario)){
        if(usuario.id != client_id){
            var chat = $('#listText')[0];
            if (aling == "izq")
                chat.innerHTML += '<li class='+aling+'>' + usuario + ': ' + texto+'</li>';
            else
                chat.innerHTML += '<li class='+aling+'>' + texto + ' :' + usuario+'</li>';
            chat.parentNode.parentNode.scrollTop = chat.parentNode.scrollHeight;
            respuestas = chat;
        }
    }
}


function annadeRespuestaLocal(usuario, texto,aling) {
    if(usuario.id != client_id){
        var chat = $('#listText')[0];
        if (aling == "izq")
            chat.innerHTML += '<li class='+aling+'>' + usuario + ': ' + texto+'</li>';
        else
            chat.innerHTML += '<li class='+aling+'>' + texto + ' :' + usuario+'</li>';
        chat.parentNode.parentNode.scrollTop = chat.parentNode.scrollHeight;
        respuestas = chat;
    }
}

function nuevaIdMensaje(){
    var res ='';
    var fecha = new Date();
    res+=ajustar(fecha.getDate());
    res+=ajustar(fecha.getMonth()+1);
    res+=fecha.getFullYear();
    res+=ajustar(fecha.getHours());
    res+=ajustar(fecha.getMinutes());
    res+=ajustar(fecha.getSeconds());
    res+=fecha.getMilliseconds();
    return res;
}

function ajustar(dato){
    if (dato>=10)
        return dato;
    else
        return '0'+dato;
}

function responder(texto) {
    var cmd = initMsn();
    cmd.channel="Data channel";
    cmd.ip = dirIP;
    cmd.type = ADD_RESPONSE;
    cmd.to = "others";
    cmd.body = texto;
    cmd.subject = "answer";
    cmd.id = nuevaIdMensaje();
    addColaChat(cmd);
    addColaSalida(cmd);
//    annadeRespuesta(usuario, texto);
}

function responderA(texto, to) {
    var cmd = initMsn();
    cmd.ip = dirIP;
    cmd.type = ADD_RESPONSE;
    cmd.to = to;
    cmd.body = texto;
    cmd.subject = "answer";
    cmd.id = idMensaje++;
    addColaChat(cmd);
    addColaSalida(cmd);
}

function estadoChat(estado){
    $(".tipoPruebas")[0].setAttribute('disabled',!estado);
}

function estadoEscritura(estado){
    var chat = $("#escritura")[0];
    chat.disabled = !estado;
    chat.readOnly = !estado;
}

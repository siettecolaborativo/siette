/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function voltear(){
    var myTable = $('#chatH')[0];
    var newTable = document.createElement('table');
    var maxColumns = 0;
    // Find the max number of columns
    for(var r = 0; r < myTable.rows.length; r++) {
        if(myTable.rows[r].cells.length > maxColumns) {
            maxColumns = myTable.rows[r].cells.length;
        }
    }
    for(var c = 0; c < maxColumns; c++) {
        newTable.insertRow(c);
        for(var r = 0; r < myTable.rows.length; r++) {
            if(myTable.rows[r].length <= c) {
                newTable.rows[c].insertCell(r);
                newTable.rows[c].cells[r] = '-';
            }
            else {
                newTable.rows[c].insertCell(r);
                newTable.rows[c].cells[r].innerHTML = myTable.rows[r].cells[c].innerHTML;
            }
        }
    }
    newTable.id = 'chatV';
    chatVertical = newTable;
    chatHorizontal = myTable;
    myTable.parentNode.appendChild(newTable);
    myTable.innerHTML = '<table id="chatH"></table>';
}

function inicVarsVolteado(){
    muestra("Inicializando variables para el volteado en vivo");
}
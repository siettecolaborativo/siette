function DesSerializar(obj) {
    var obj2;
    try{
        obj2 = JSON.parse(obj);
    }catch (err){
        obj2=obj;
    }finally{
        if (obj2.message != null) {
            desMessage(obj2.message);
        } else if (obj2.command != null) {
            desCommand(obj2.command);
        } else if (obj2.messageContainer != null) {
            desMessages(obj2.messageContainer.messages);
        } else {
            fallo();
        }
    }
}

function fallo() {
    muestra('fallo durante el desserializado del objeto JSON');
}

function desMessages(mensajes) {
    for (var i = 0; i < mensajes.length; i++) {
        DesSerializar(mensajes[i]);
    }
}

function desMessage(mensaje) {
    addColaEntrada(new Message(mensaje));
}

function desCommand(comando) {
    addColaEntrada(new Command(comando));
}

function addUserTo(to){
    var cmd = initMsn();
    cmd.channel = "Cmd channel"
    cmd.ip =  dirIP
    cmd.type = ADD_USER;
    cmd.to = to;
    cmd.subject = (new Command()).getAsString(ADD_USER);
    addColaSalida(cmd);
}

function esperoRespuestas() {
    var cmd = initMsn();
    cmd.channel = "Cmd channel";
    cmd.ip =  dirIP;
    cmd.type = WAIT_RESPONSES;
    cmd.to = "others";
    cmd.subject = (new Command()).getAsString(WAIT_RESPONSES);
    addColaSalida(cmd);
}

function prepararReceive(){
    var cmd = initCmd();
    cmd.ip= dirIP;
    cmd.type = RECEIVE;
    addColaSalida(cmd);
}

//funcion para enviar un update user status
function prepararUpdateUserStatus(){
    var msn = initMsn();
    msn.channel = "Cmd channel";
    msn.type = UPDATE_USER_STATUS;
    msn.to = "others";
    msn.subject = UPDATE_USER_STATUS;
    msn.ip = dirIP;
    return msn;
}

function preparaACK() {
    var cmd = initCmd();
    cmd.ip = dirIP;
    cmd.type = ACK;
    addColaSalida(cmd);
}

function prepararNACK(){
    var cmd = initCmd();
    cmd.body = 'El cliente no ha entendido el mensaje recibido por el servidor';
    cmd.ip = dirIP;
    cmd.type = NACK;
    addColaSalida(cmd);
}

function initCmd() {
    var obj = new Command();
    obj.initCommand(this.client_id, this.usuarioLocal, this.asignaturaActual);
    return obj;
}

function initMsn() {
    var obj = new Message();
    obj.initMessage(this.client_id, this.usuarioLocal, this.asignaturaActual);
    var user = listaUsuarios[getIndexUser(client_id)];
    obj.item = item;
    obj.step = step;
    obj.session = asignaturaActual;
    obj.client_name = usuarioLocal;
    obj.ip = dirIP;
    obj.id = nuevaIdMensaje();
    return obj;
}

function prepararSalida() {
    var res = '';
    var aux = false;
    /*
    if (colaSalida.size() > 1) {
        res += '{"messageContainer":{"messages":[';
        aux = true;
    }
    var i;
    for (i = 0; i < colaSalida.size(); i++) {
        res += colaSalida.get(i).getJSON();
        if (aux && ((colaSalida.size() - 1) != i))
            res += ',';
        else if (aux)
            res += ']}}';
    }*/
    res+=colaSalida.getFirst().getJSON();
    return res;
}
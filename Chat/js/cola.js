var Cola = function ()
{
    this.lista = [];

    this.add = function (data) {
        this.lista.push(data);
    };

    this.get = function (i) {
        return this.lista[i];
    };

    this.getDel = function (i) {
        var obj = this.get(i);
        this.lista.splice(i, 1);
        return obj;
    };

    this.getFirst = function () {
        return this.get(0);
    };

    this.removeFirst = function () {
        this.lista.splice(0, 1);
    };

    this.removeN = function (n) {
        for (var i = 0; i < n; i++)
            this.removeFirst();
    };

    this.getDelFirst = function () {
        return this.getDel(0);
    };

    this.getN = function (n) {
        if (n > this.size())
            n = this.size();
        else if (n < 0)
            n = 0;
        return this.lista.slice(0, n);
    };

    this.confirmaN = function (n) {
        this.removeN(n);
    };

    this.toString = function () {
        return this.lista.toString();
    };

    this.isEmpty = function () {
        return (this.lista.length == 0);
    };

    this.size = function () {
        return this.lista.length;
    };

    this.getLista = function () {
        return this.lista;
    };
};

function initColas() {
    colaSalida = new Cola();
    colaEntrada = new Cola();
    colaChat = new Cola();
    listaUsuarios = new Cola();
    if (testMode){
        colaMessages = new Cola();
    }
}


function addColaMessages(obj){
    colaMessages.add(obj);
}

function addColaSalida(obj) {
    colaSalida.add(obj);
}

function addColaEntrada(obj) {
    colaEntrada.add(obj);
}

function addColaChat(obj) {
    colaChat.add(obj);
}

function addColaUsuario(us){
    listaUsuarios.add(us);
}
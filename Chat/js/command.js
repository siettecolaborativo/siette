var Command = function(obj){
    this.id=null;
    this.client_id=null;
    this.client_name=null;
    this.session=null;
    this.channel=null;
    this.ip=null;
    this.error=null;
    this.type=null;
    
    this.toString = function(){
        var st = "";
        st += "id = " +this.id + " ";
        st += "client_id = " +this.client_id + " ";
        st += "client_name = " +this.client_name + " ";
        st += "session = " +this.session + " ";
        st += "channel = " +this.channel + " ";
        st += "ip = " +this.ip + " ";
        st += "error = " +this.error + " ";
        return st+" type = "+ this.getAsString(this.type);
    };
    
    this.initCommand = function(client_id, client_name, session) {
        this.session = session;
        this.client_id = client_id;
        this.client_name = client_name;
    };
    
    this.setType = function(type){
        this.type=type;
    };
    
    this.getType= function(){
            return this.type;
    };

    this.getId = function() {
        return this.id;
    };

    this.setId = function(id) {
        this.id = id;
    };

    this.getChannel = function() {
        return this.channel;
    }; 

    this.getClientId=function() {
        return this.client_id;
    };
    
    this.getClientName=function() {
        return this.client_name;
    };

    this.getClient = function() {
    return  new Client(this.client_id,this.client_name);
    };

    this.getSession =function() {
        return this.session;
    };

    this.getError=function () {
        return this.error;
    };

    this.getIp=function() {
        return this.ip;
    };

    this.setChannel=function(channel) {
        this.channel = channel;
    };

    this.setIp=function(ip) {
        this.ip = ip;
    };

    this.setClientClase = function (client) {
        this.client_name = client.getName();
        this.client_id = client.getId();
    };
    
    this.setClient = function (client_id, client_name) {
        this.client_name = client_name;
        this.client_id = client_id;
    };
    
    this.setClientId = function (client_id) {
        this.client_id = client_id;
    };
    
    this.setClientName=function(client_name) {
        this.client_name = client_name;
    };

    this.setSession=function(session) {
        this.session = session;
    };

    this.setContext=function(type) {
        this.dataType = type;
    };

    this.setError=function (error) {
        this.error = error;
    };
    
    this.put = function(str, da) {
        return "\"" + str + "\"" + ":" + "\"" + da + "\"";
    };
    
    this.getJSON = function (){
        var res='{"command":{';
        res+=this.put("id",this.id)+",";
        res+=this.put("client_id",this.client_id)+",";
        res+=this.put("client_name",this.client_name)+",";
        res+=this.put("session",this.session)+",";
        res+=this.put("channel",this.channel)+",";
        res+=this.put("ip",this.ip)+",";
        res+=this.put("error",this.error)+",";
        res+=this.put("type",this.type)+"}}";
        return res;
    };



    this.getAsString= function(cmd) {
        cmd = parseInt(cmd);
        switch (cmd) {
            case OPEN_SESSION :
                return "open_session";
                break;
            case CLOSE_SESSION :
                return "close_session";
                break;
            case GET_SESSION :
                return "get_session";
                break;
            case ADD_CLIENT :
                return "add_client";
                break;
            case REMOVE_CLIENT :
                return "remove_client";
                break;
            case CREATE_CHANNEL :
                return "create_channel";
                break;
            case REMOVE_CHANNEL :
                return "remove_channel";
                break;
            case GET_CHANNEL :
                return "get_channel";
                break;
            case JOIN :
                return "join";
                break;
            case LEAVE :
                return "leave";
                break;
            case SEND :
                return "send";
                break;
            case RECEIVE :
                return "receive";
                break;
            case CLIENT_DISCONNECTED :
                return "client_desconnected";
                break;
            case ACK :
                return "ack";
                break;
            case NACK :
                return "nack";
                break;
            case INIT :
                return "init";
                break;
            case END :
                return "end";
                break;
            case ADD_USER :
                return "add_user";
                break;
            case REMOVE_USER :
                return "remove_user";
                break;
            case WAIT_RESPONSES :
                return "wait_responses";
                break;
            case UPDATE_USER_STATUS :
                return "update_user_status";
                break;
            case ADD_RESPONSE :
                return "add_response";
                break;
            case END_TEST :
                return "end_test";
                break;
            default :
                return "unknown";
         }
    };
    for (var prop in obj) this[prop] = obj[prop];
};
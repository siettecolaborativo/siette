function limpiarSalida() {
    if (colaSalida.isEmpty()) {
        prepararReceive();
        recibeJson();
    } else {
        recibeJson();
    }
}
//setInterval('limpiarSalida()', 1000);

function limpiarEntrada() {
    while (!colaEntrada.isEmpty()) {
        var obj = colaEntrada.getDelFirst();
        var tipo = parseInt(obj.type);
        switch (tipo) {
            case ADD_USER:
                if (testMode){
                    cargarUsuario(obj.client_name, obj.client_id, obj.item, obj.step);
                }else{
                    cargarUsuario(obj.client_name, obj.client_id);
                }
                break;
            case ADD_RESPONSE:
                if (obj.to == "all"){
                	if (testMode){
                		if (step == 2 && obj.item == item){
                			annadeRespuesta(obj.client_name, obj.body.replace(/[+]/g, " "),'izq',obj.id,obj.client_id);
                		}else{
                			addColaMessages(obj);
                		}
                	}else{
                		annadeRespuesta(obj.client_name, obj.body.replace(/[+]/g, " "),'izq',obj.id,obj.client_id);
                	}
                }else{
                    annadeRespuesta(obj.client_name, obj.body.replace(/[+]/g, " "),'izq',obj.id,obj.client_id);
                }
                break;
            case SEND:
                if (obj.to == "all"){
                    annadeRespuesta(obj.client_name, obj.body.replace(/[+]/g, " "),'izq',obj.id,obj.client_id);
                }else{
                    annadeRespuesta(obj.client_name, obj.body.replace(/[+]/g, " "),'izq',obj.id,obj.client_id);
                }
                break;
            case REMOVE_USER:
                eliminarUsuario(obj.client_name, obj.client_id);
                break;
            case WAIT_RESPONSES:
                addUserTo(obj.client_id);
                break;
            case ACK:
                manejoACK(obj);
                break;
            case NACK:
                manejoNACK(obj);
                break;
            case UPDATE_USER_STATUS:
                manejoUpdateUserStatus(obj);
                break;
        }
    }
}

function limpiarColaChat() {
    var aux = colaChat.getDelFirst();
    if (aux.to == "all" || aux.to == "others")
        annadeRespuestaLocal(aux.client_name, aux.body.replace(/[+]/g, " "),'der');
    else
        annadeRespuestaLocal(aux.client_name, aux.body.replace(/[+]/g, " "),'der');
}

function confirmaMen() {
    var obj = colaSalida.getFirst();
    var tipo = parseInt(obj.type);
    switch (tipo) {
        case ADD_RESPONSE:
            colaSalida.getDelFirst();
            limpiarColaChat();
            break;
        case INIT:
            colaSalida.getDelFirst();
            esperoRespuestas();
            break;
        case WAIT_RESPONSES:
            if (!testMode){
                estadoChat(true);
                estadoEscritura(true);
            }
            colaSalida.getDelFirst();
            break;
        default :
            colaSalida.getDelFirst();
    }
}

function manejoNACK(obj){
    muestra("Ha llegado un NACK");
}

function manejoACK(obj){
    muestra("Ha llegado un ACK");
}

function limpiarACKSalida(){
    var colaAux = new Cola();
    for (var i = 0; i < colaSalida.size(); i++){
        if(colaSalida.get(i).type == ACK){
            colaAux.add(i);
        }
    }
    for (var i = colaAux.size()-1; i>=0 ;i--){
        colaSalida.getDel(colaAux.get(i));
    }
}

function cleanQueueMessages(){
	var colaAux = new Cola();
	for(var i = 0; i< colaMessages.size(); i++){
		var obj = colaMessages.get(i);
		if (obj.item > item){
			colaAux.add(obj);
		}else if (obj.item == item){
			annadeRespuesta(obj.client_name, obj.body.replace(/[+]/g, " "),'izq',obj.id,obj.client_id);
		}
	}
	colaMessages = new Cola();
	for(var i = 0; i< colaAux.size(); i++){
		addColaMessages(colaAux.get(i));
	}
}

// Funtion tu manage a update user status
function manejoUpdateUserStatus(obj){
    jsSetStateUser(obj.client_id, obj.item, obj.step);
}

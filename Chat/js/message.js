var Message = function(obj) {
    this.id = null;
    this.client_id = null;
    this.client_name = null;
    this.session = null;
    this.channel = null;
    this.type=null;
    this.ip = null;
    this.error = null;
    this.to = null;
    this.reply_to = null;
    this.subject = null;
    this.body = null;
    this.item = ITEM_NULL;
    this.step = STEP_NULL;


    this.getJSON = function() {
        var res = '{"message":{';
        res += this.put("id", this.id) + ",";
        res += this.put("client_id", this.client_id) + ",";
        res += this.put("client_name", this.client_name) + ",";
        res += this.put("session", this.session) + ",";
        res += this.put("channel", this.channel) + ",";
        res += this.put("ip", this.ip) + ",";
        res += this.put("error", this.error) + ",";
        res += this.put("type", this.type) + ",";
        res += this.put("to", this.to) + ",";
        res += this.put("reply_to", this.reply_to) + ",";
        res += this.put("subject", this.subject) + ",";
        res += this.put("body", this.body) + ",";
        res += this.put("item", this.item) + ",";
        res += this.put("step", this.step) + "}}";
        return res;
    };

    this.put = function(str, da) {
        return "\"" + str + "\"" + ":" + "\"" + da + "\"";
    };

    this.toString = function() {
        var st = "";
        st += "to = " + this.to + " ";
        st += "reply_to = " + this.reply_to + " ";
        st += "subject = " + this.subject + " ";
        st += "body = " + this.body + " ";
        st += "item = " + this.item + " ";
        st += "step = " + this.step + " ";
        st += "id = " + this.id + " ";
        st += "client_id = " + this.client_id + " ";
        st += "client_name = " + this.client_name + " ";
        st += "session = " + this.session + " ";
        st += "channel = " + this.channel + " ";
        st += "ip = " + this.ip + " ";
        st += "error = " + this.error + " ";
        var aux = new Command();
        aux.setType(this.type);
        st += "type = " + aux.getAsString() + " ";
        return  st;
    };
    
    this.initMessage = function(client_id, client_name, session) {
        this.session = session;
        this.client_id = client_id;
        this.client_name = client_name;
    };

    this.InicMessage = function(to, subject, body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    };

    this.InicMessageBody = function(to, reply_to, subject, body) {
        Message(to, subject, body);
        this.reply_to = reply_to;
    };

    this.getBody = function() {
        return this.body;
    };

    this.getReplyTo = function() {
        return this.reply_to;
    };

    this.getSubject = function() {
        return this.subject;
    };

    this.getTo = function() {
        return this.to;
    };

    this.setBody = function(body) {
        this.body = body;
    };

    this.setReplyTo = function(reply_to) {
        this.reply_to = reply_to;
    };

    this.setSubject = function(subject) {
        this.subject = subject;
    };

    this.setTo = function(to) {
        this.to = to;
    };

    this.getType = function() {
        return this.type;
    };

    this.setType = function(type) {
        this.type = type;
    };

    this.setData = function(data) {
        this.datos = data;
    };

    this.getData = function() {
        return this.datos;
    };

    this.setDataType = function(type) {
        this.dataType = type;
    };

    this.getId = function() {
        return this.id;
    };

    this.setId = function(id) {
        this.id = id;
    };

    this.getChannel = function() {
        return this.channel;
    };

    this.getClientId = function() {
        return this.client_id;
    };

    this.getClientName = function() {
        return this.client_name;
    };

    this.getClient = function() {
        return  new Client(this.client_id, this.client_name);
    };

    this.getSession = function() {
        return this.session;
    };

    this.getDataType = function() {
        return this.dataType;
    };

    this.getError = function() {
        return this.error;
    };

    this.getIp = function() {
        return this.ip;
    };

    this.setChannel = function(channel) {
        this.channel = channel;
    };

    this.setIp = function(ip) {
        this.ip = ip;
    };

    this.setClientClase = function(client) {
        this.client_name = client.getName();
        this.client_id = client.getId();
    };

    this.setClient = function(client_id, client_name) {
        this.client_name = client_name;
        this.client_id = client_id;
    };

    this.setClientId = function(client_id) {
        this.client_id = client_id;
    };

    this.setClientName = function(client_name) {
        this.client_name = client_name;
    };

    this.setSession = function(session) {
        this.session = session;
    };

    this.setContext = function(type) {
        this.dataType = type;
    };

    this.setError = function(error) {
        this.error = error;
    };
    for (var prop in obj) this[prop] = obj[prop];
};

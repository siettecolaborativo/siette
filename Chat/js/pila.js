var node = function()
{
    var data;
    var next = null;
};

var Stack = function()
{
    this.top = null;
    this.cant = 0;

    this.push = function(data) {
        if (this.top == null) {
            this.top = new node();
            this.top.data = data;
        } else {
            var temp = new node();
            temp.data = data;
            temp.next = this.top;
            this.top = temp;
        }
        this.cant++;
    };

    this.pop = function() {
        var temp = this.top;
        var data = this.top.data;
        this.top = this.top.next;
        temp = null;
        this.cant--;
        return data;
    };

    this.isEmpty = function() {
        return (this.cant==0);
    };
    
    this.size = function(){
        return this.cant;
    };
};
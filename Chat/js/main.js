var usuarioLocal = "Root";
var client_id;
var id;
var item = -1;
var step = -1;
var asignaturaActual;
var URL = "SalaServlet";
var dirIP = "127.0.0.1";

var contReenvio = 0;
var ultMensaje = '';
var listaUsuarios;


var colaSalida; // It is the queue that manage all messages that send the client to the server
var colaEntrada;// It is a queue for receive message from the server
var colaChat;// Only for messages(A message need to be confirmed by the server to get out the queue)
var colaMessages;//Only for save the message in the test mode

//Variables de idioma del chat
var tituloChat;
var estadoUser;
var listUser;
var tituloChatUser;
var respondera;
var todos;
var todasRespuestas;
var placeholderchat;
var placeholderescritura;
//Estados en las preguntas colaborativas
var fase1;
var fase2;
var fase3;

//Variables manejo salida por consola
var debug = 0;
var alerta = 0;

//Variables para volteado del chat y para el mantenimiento de la informacion para el volteado en vivo
var volteado = 1;
var chatHorizontal = '';
var chatVertical = '';
var respuestas = '';
var listaUsers = '';
var desplegableUsers = '';

//variables generables
var debugAvanzado = false;
var timeOut = 2000;
var timeOutActual;

//variables modo test
var testMode= false;


var idMensaje = 0;
//Funcion de inicializacion de la pagina
function inicializacion() {
    console.log('Disable test mode');
    //disable test mode
    testMode = false;
    //init variables of the chat (translation, client_ids ....)
    console.log('loading params');
    timeOutActual = timeOut;
    URL = getParamHTML('URL');
    tituloChat = getParamHTML('term5');
    estadoUser = getParamHTML('term0');
    listUser = getParamHTML('term9');
    tituloChatUser = getParamHTML('term7');
    respondera = getParamHTML('term12');
    todos = getParamHTML('term26');
    todasRespuestas = getParamHTML('term6');
    placeholderchat = getParamHTML('term27');
    placeholderescritura = getParamHTML('term28');
    inicLanguaje();
    initColas();
    usuarioLocal = getParamHTML('username');
    client_id = getParamHTML('userid');
    asignaturaActual = getParamHTML('session');
    //loading data of url
    console.log('loaing url params');
    leeUrl();
    console.log('init vars');
    //init the item and step to null
    item = ITEM_NULL;
    step = STEP_NULL;
    id = client_id;
    
    var us = new Usuario();
    us.nombre = usuarioLocal;
    us.id = client_id;
    inicVarsVolteado();
    if (debugAvanzado)
        initDebugAvanzado();
    if (volteado == 1)
        voltear();
    console.log('loading user');
    cargarUsuario(us.nombre,us.id);
    estadoChat(false);
    estadoEscritura(false);
    console.log('creating init command');
    initSesionServer();
    manejaTodasRes(false);
    console.log('end');
}

function inicializacionTestMode() {
    console.log('Enable test mode');
    // init test mode
    testMode = true;
    //init variables of the chat (translation, client_ids ....)
    console.log('loading params');
    timeOutActual = timeOut;
    URL = getParamHTML('URL');
    tituloChat = getParamHTML('term5');
    estadoUser = getParamHTML('term0');
    listUser = getParamHTML('term9');
    tituloChatUser = getParamHTML('term7');
    respondera = getParamHTML('term12');
    todos = getParamHTML('term26');
    todasRespuestas = getParamHTML('term6');
    placeholderchat = getParamHTML('term27');
    placeholderescritura = getParamHTML('term28');
    inicLanguaje();
    initColas();
    usuarioLocal = getParamHTML('username');
    client_id = getParamHTML('userid');
    fase1 = getParamHTML('term1');
    fase2 = getParamHTML('term2');
    fase3 = getParamHTML('term3');
    //init variables del test para los mensajes
    console.log('init vars');
    item = 1;
    step = 1;
    //loading data of url
    console.log('loaing url params');
    leeUrl();
    id = client_id;
    asignaturaActual = getParamHTML('session');
    var us = new Usuario();
    us.nombre = usuarioLocal;
    us.id = client_id;
    us.item = item;
    us.step = step;
    inicVarsVolteado();
    if (debugAvanzado)
        initDebugAvanzado();
    if (volteado == 1)
        voltear();
    console.log('loading user');
    cargarUsuario(us.nombre,us.id,us.item);
    estadoChat(false);
    estadoEscritura(false);
    console.log('creating init command');
    initSesionServer();
    manejaTodasRes(false);
    console.log('end');
}


function getParamHTML(name){
    return $('param[name='+name+']')[0].value;
}

function manejaTodasRes(estado){
    if (estado){
        $('.todasRes')[0].setAttribute('style','display:block;');
    }else{
        $('.todasRes')[0].setAttribute('style','display:none;');
    }
}

function initSesionServer() {
    var cmd = initCmd();
    cmd.ip = '127.0.0.1';
    cmd.type = INIT;
    addColaSalida(cmd);
    //recibeJson();
}

//lectura valores url
function getGET() {
    // capturamos la url
    var loc = document.location.href;
    return getParam(loc, "get");
}

function getParam(loc, tipo) {
    // si existe el interrogante
    var GET;
    var enc = false;
    if (tipo == "get") {
        if (loc.indexOf('?') > 0) {
            enc = true;
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            GET = getString.split('&');
        }
    } else if (tipo == "post") {
        GET = loc.split('&');
        enc = true;
    }
    var get = {};
    if (enc) {
        // recorremos todo el array de valores
        for (var i = 0, l = GET.length; i < l; i++) {
            var tmp = GET[i].split('=');
            get[tmp[0]] = unescape(decodeURI(tmp[1]));
        }
    }
    return get;
}

function leeUrl() {
    // Cogemos los valores pasados por get
    var valores = getGET();
    if (valores)
    {
        // hacemos un bucle para pasar por cada indice del array de valores
        for (var index in valores)
        {
            if (index == "debug") {
                debug = valores[index];
                if (debug == 1)
                    muestra("Modo debug activado");
                else
                    muestra("Modo debug desactivado");
            } else if (index == "alert") {
                alerta = valores[index];
                if (alerta == 1)
                    muestra("Modo alerta activado");
                else
                    muestra("Modo alerta desactivado");
            } else if (index == "debugAvanzado") {
                debugAvanzado = true;
            } else if (index == "volteado") {
                volteado = valores[index];
                if (volteado == 1)
                    muestra("El chat se inicializara en formato vertial");
                else
                    muestra("El chat se inicializara en formato horizontal");
            } else if (index == "usuario") {
                usuarioLocal = valores[index];
            } else if (index == "id") {
                client_id = valores[index];
            } else if (index == "session") {
                asignaturaActual = valores[index];
            }
        }
    } else {
        // no se ha recibido ningun parametro por GET
        muestra("No se ha recibido ningún parámetro");
    }
}

//Salida del chat

function inicLanguaje() {
    //document.getElementById("tituloChat").innerHTML = tituloChat;
    $("#EstadoUsuarios")[0].innerHTML += listUser;
    //$("#listaUsuarios")[0].innerHTML = listUser;
    $("#tituloChatUser")[0].innerHTML += tituloChatUser;
    $("#envio").text(todasRespuestas);
    //document.getElementById("chat").placeholder = placeholderchat;
    $("#escritura")[0].placeholder = placeholderescritura;
}
$( window ).bind( 'beforeunload', function() {
    finaliza();
} );

function finaliza() {
    var cmd = new Command();
    cmd.client_id = client_id;
    cmd.client_name = usuarioLocal;
    cmd.session = asignaturaActual;
    cmd.channel = '';
    cmd.ip = '127.0.0.1';
    cmd.type = END;
    recibeJsonUrgente(cmd);
}

// pulsar intro para enviar
$(document).keypress(function (e) {
    if (e.which == 13) {
        respuesta();
    }
});


var Usuario = function() {
    this.nombre;
    this.id;
    this.item;
    this.step;
    this.listaId = [];
    this.estado=0;
};
var estado1='33.333333%';
var estado2='66.666666%';
var estado3='100%';
var classbefore = "label-info";
var classEquals = "label-success";
var classAfter = "label-danger";
var classProgressBar = 'progress-bar progress-bar-warning progress-bar-striped';

// function to set the status to local user
function jsSetStateLocal(item_set, step_set){
    var msn = prepararUpdateUserStatus();
    msn.item = item_set;
    msn.step = step_set;
    item = item_set;
    step = step_set;
    if (step==2){
        estadoChat(true);
        estadoEscritura(true);
        cleanQueueMessages();
    }else{
        estadoChat(false);
        estadoEscritura(false);
    }
    updateUserTest(client_id, usuarioLocal, item, step);
    addColaSalida(prepararUpdateUserStatus());
}

//function to procces a set status
function jsSetStateUser(client_id, item, step){
    var user = getUser(client_id);
    user.item = item;
    user.step = step;
    updateUserTest(client_id, user.nombre, item, step);
}

function updateUsersTestState(){
    for (var i = 0; i < listaUsuarios.size();i++){
        var user = listaUsuarios.get(i);
        updateUserTest(user.id, user.nombre, user.item, user.step);
    }
}

function updateUserTest(client_id, client_name, item, step){
    updateItem(client_name, client_id, item);
    updateStep(client_id,client_name,step);
}

function updateStep(client_id, client_name, step){
    var bar = $('#'+client_name+'\\:'+client_id)[0];
    var newBar = getProgressBar(client_id, client_name,step);
    bar.innerHTML = newBar;
}

function updateItem(nombre, id, item){

    $('#item\\:'+nombre+'\\:'+id)[0].innerHTML = item != null ? item : nextItem(id);
}

function getProgressBar(client_id, client_name, step){
    var textStep;
    var width;
    if (step == 1){
        textStep = fase1;
        width = estado1;
    }else if (step == 2){
        textStep = fase2;
        width = estado2;
    }else{
        textStep = fase3;
        width = estado3;
    }
    return '<div style="width: 70%;" id="'+client_name+':'+client_id+'"><div class="progress" style="width:80%;margin-left:10px;"><div data-id="'+client_name+':'+client_id+'" class="'+classProgressBar+'" role="progressbar" aria-valuenow="33.333" aria-valuemin="0" aria-valuemax="100" style="width:'+width+'"><span>'+textStep+'</span></div></div></div>';
}

function getBadge(client_id, client_name, item_aux){
    var classColor = '';
    if (item > item_aux){
        classColor = classbefore;
    }else if (item == item_aux){
        classColor = classEquals;
    }else{
        classColor = classAfter;
    }
    return '<span id="step:'+client_id+'"><span id="item:'+client_name+':'+client_id+'" class="item label '+classColor+'">'+item_aux+'</span></span>';
}

function getUser(client_id, client_name){
    if (client_id){
        return getUserById(client_id);
    }else{
        return getUserByName(client_name);
    }
}

function getUserByName(client_name){
    var indice = 0;
    var res = -1;
    while(indice < listaUsuarios.size()){
        var usr_aux = listaUsuarios.get(indice);
        if(usr_aux.nombre == client_name){
            return usr_aux;
        }
        indice++;
    }
}

function getUserById(client_id){
    var indice = 0;
    var res = -1;
    while(indice < listaUsuarios.size()){
        var usr_aux = listaUsuarios.get(indice);
        if(usr_aux.id == client_id){
            return usr_aux;
        }
        indice++;
    }
}

function getIndexUser(id){
    var indice = 0;
    var res = -1;
    while(indice < listaUsuarios.size()){
        if(listaUsuarios.get(indice).id == id){
            res = indice;
            break;
        }
        indice++;
    }
    return res;
}

function getNombre(id){
    var indice = 0;
    var res;
    while(indice < listaUsuarios.size()){
        if(listaUsuarios.get(indice).id == id){
            res = listaUsuarios.get(indice).nombre;
            break;
        }
        indice++;
    }
    return res;
}

function nextItem(id){
    var indice = 0;
    var res;
    while(indice < listaUsuarios.size()){
        if(listaUsuarios.get(indice).id == id){
            res = listaUsuarios.get(indice);
            res.item++;
            break;
        }
        indice++;
    }
    return res.item;
}

function repetido(id,usuario,id_usuario){
    for(var i = 0; i< listaUsuarios.size(); i++){
        if (listaUsuarios.get(i).id==id_usuario){
            var us = listaUsuarios.get(i);
            for (var j = 0; j< us.listaId.length;j++){
                if (us.listaId[j]==id)
                    return true;
            }
            return false;
        }
    }
    return false;
}

function annadirUsuarioConEstado(nombre, estado) {
    var usuarios = $("#001")[0];
    var res = usuarios.innerHTML.substring("0", usuarios.innerHTML.indexOf("</ul>"));
    res += '<li class="lineaUser"><span>' + nombre + '<span><span class="individual">' + estado + '</span></li></ul>';
    usuarios.innerHTML = res;
    listaUsers = usuarios;
}

function annadirUsuario(nombre,id,item) {
    //load the structure of user
    var usuarios = $("#001")[0];
    var res = usuarios.innerHTML.substring("0", usuarios.innerHTML.indexOf("</ul>"));
    //add a new li with the user
    res += '<li class="lineaUser"><span style="display:flex;"><icon class="glyphicon glyphicon-user"></icon>' + nombre;
    //Progress Bar
    res += testMode ? getBadge(id,nombre,item)+getProgressBar(id,nombre,1) : '';
    //close the structure
    res += '</span></li></ul>';
    //update the box with the student
    usuarios.innerHTML = res;
    listaUsers = usuarios;
}

function cargarUsuario(nombre,id,item,step){
    if (!estaUsuario(id)){
        var us = new Usuario();
        us.nombre = nombre;
        us.id = id;
        //si estamos en el test anadimos el paso por el que va el usuario
        if (testMode){
            us.item = item;
            us.step = step;
        }
        us.listaId = [];
        addColaUsuario(us);
        if (testMode){
            annadirUsuario(us.nombre,us.id,us.item);
        }
        else{
            annadirUsuario(us.nombre,us.id);
        }
        muestra("Se ha a&nacute;adido el usuario "+id+" --> "+ nombre + " al listado de usuarios");
    }else{
        muestra("El usuario "+id+" --> "+ nombre+" ya se encuentra en la lista de usuarios");
    }
}

function estaUsuario(id){
    for(var i = 0; i< listaUsuarios.size(); i++){
        if (listaUsuarios.get(i).id==id)
            return true;
    }
    return false;
}


function actualizaBox() {
   /* var aux = document.getElementById("004");
    var esc = '<span><img src="img/reply_img.png" /><span class="spanRespondera">'+respondera+'</span></span><select id="listUser" class="seleccion" name="userss"><option selected value="all">'+todos+'</option>';
    var ind = 0;
    while (ind < listaUsuarios.size()) {
        var userAux = listaUsuarios.get(ind++);
        if (userAux.id != client_id)
            esc += '<option value=' + userAux.nombre + '>' + userAux.nombre + '</option>';
    }
    esc += '</select>';
    aux.innerHTML = esc;
    desplegableUsers = aux;
    */
}

function cambiarEstadoUsuario(nombre, estado, pos) {
    var state = $("#"+nombre + "estado")[0];
    state.innerHTML = estado;
    if (pos == -1)
        estado = estado + 'p';
    if (pos == 1)
        estado = estado + 'd';
    state.className = estado;
}

function eliminarUsuario(name,id) {
    var usuarios = $("#001")[0];
    usuarios.innerHTML = '<ul class="listaUser">'+listUser+'</ul>';
    var ind = 0;
    var arrayAux = new Cola();
    while (ind < listaUsuarios.size()) {
        var userAux = listaUsuarios.get(ind++);
        if (userAux.nombre != name && userAux.id != id) {
            arrayAux.add(userAux);
            annadirUsuario(userAux.nombre,userAux.id);
        }
    }
    listaUsuarios = new Cola();
    for(ind=0;ind<arrayAux.size();ind++)
        addColaUsuario(arrayAux.get(ind));
}
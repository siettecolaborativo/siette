/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function limpiarSalidaSimulada(){
     if (colaSalida.isEmpty()) {
        esperoRespuestas();
    }
    actualizaColas();
}

function paso1JSON() {
    muestra("Iniciada transferencia ajax");
    var temp = prepararSalida();
    muestra("Intento de envio de ->" + temp);
    actualizaColas();
    actualizaOut(temp);
}

function actualizaIn(texto){
    var textIn = $('#in')[0];
    textIn.innerHTML = texto;
}

function actualizaOut(texto){
    var textIn = $('#out')[0];
    textIn.innerHTML = texto;
}

function desSerializaIn(){
    var textIn = $('#in')[0];
    DesSerializar(textIn.value);
}

function paso2JSON(respu, ack) {
    if (respu) {
        var temp2;
        if (ack) {
            temp2 = getACK().getJSON();
        } else {
            temp2 = getNACK().getJSON();
        }
        muestra("Se ha recibido la siguiente respuesta -> " + temp2);
        DesSerializar(temp2);
        limpiarACKSalida();
        actualizaIn(temp2);
    } else {
        muestra("Fallo con el servidor");
    }
    actualizaColas();
}
function paso3JSON() {
    limpiarEntrada();
    actualizaColas();
}
function recibeJson() {
    muestra("Iniciada transferenci ajax");
    var temp = encodeURIComponent(prepararSalida());
    var temp2 = getACK().getJSON();
    muestra("Transferencia satisfactoria se ha enviado -> " + decodeURIComponent(temp));
    muestra("Se ha recibido la siguiente respuesta -> " + temp2);
    if (temp2 != undefined) {
        DesSerializar(temp2);
        limpiarEntrada();
    }
    actualizaColas();
}

function getACK() {
    var cmd = initCmd();
    cmd.ip = '127.0.0.1';
    cmd.type = ACK;
    return cmd;
}
function getNACK(){
    var cmd = initCmd();
    cmd.ip = '127.0.0.1';
    cmd.type = NACK;
    return cmd;
}
function getADD_USER(){
    var obj = new Command();
    obj = initCmd();
    obj.client_id = 2;
    obj.client_name = 'Juan';
    obj.ip = '127.0.0.1';
    obj.type = ADD_USER;
    return obj;
}

function getREMOVE_USER(){
    var obj = new Command();
    obj = initCmd();
    obj.client_id = 2;
    obj.client_name = 'Juan';
    obj.ip = '127.0.0.1';
    obj.type = REMOVE_USER;
    return obj;
}

function actualizaColas() {
    var cola = $("#colaEntrada")[0];
    var obj = new Command();
    var aux = '<h2>Entrada</h2>';
    for (var i = 0; i < colaEntrada.size(); i++)
        aux += '<li>' + obj.getAsString(colaEntrada.get(i).type) + '</li>';
    cola.innerHTML = aux;
    cola = $("#colaSalida")[0];
    aux = '<h2>Salida</h2>';
    for (var i = 0; i < colaSalida.size(); i++)
        aux += '<li>' + obj.getAsString(colaSalida.get(i).type) + '</li>';
    cola.innerHTML = aux;
    cola = $("#colaChat")[0];
    aux = '<h2>Chat</h2>';
    for (var i = 0; i < colaChat.size(); i++)
        aux += '<li>' + obj.getAsString(colaChat.get(i).type) + '</li>';
    cola.innerHTML = aux;
    aux = '';
    cola = $("#listaUsuarios")[0];
    aux = '<h2>ListaUsuarios</h2>';
    for (var i = 0; i < colaChat.size(); i++)
        aux += '<li>' + obj.getAsString(listaUsuarios.get(i).nombre+" "+ listaUsuarios.get(i).id) + '</li>';
    cola.innerHTML = aux;
}
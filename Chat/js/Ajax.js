function recibeJson() {
    var hrq = false;
    if (window.XMLHttpRequest) {// Mozilla/Chrome 
        hrq = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {// Internet Explorer
        hrq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (hrq) {
        hrq.open('POST', URL, true);
        hrq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        muestra('Se ha enviado al servidor --> '+colaSalida.getFirst().getJSON());
        var mensaje = encodeURIComponent(prepararSalida());
        hrq.send("data="+mensaje);
        hrq.onreadystatechange = function () { // Callback
            if (hrq.readyState == 4) {
                muestra("Transferencia ajax terminada");
                if (hrq.status == 200) {
                    var params = getParam(hrq.responseText, "post");
                    var data = "";
                    if (params) {
                        // hacemos un bucle para pasar por cada indice del array de valores
                        for (var p in params) {
                            if (p == "data") {
                                data = decodeURIComponent(params[p]);
                            }
                        }
                    }
                    muestra("Se ha recibido del servidor --> "+ data);
                    muestra("Transferencia ajax terminada correctamente");
                    var tam = colaEntrada.size();
                    var tam2;
                    DesSerializar(data);
                    tam2 = colaEntrada.size();
                    if (tam < tam2){
                        confirmaMen();
                        timeOutActual = timeOut;
                        //setTimeout(limpiarSalida(), 20000);
                    }
                    //limpiarACKSalida();
                    limpiarEntrada();  
                } else{
                    timeOutActual *=2;
                    muestra('Transferencia ajax fallida');
                    muestra('Fallo al enviar el mensaje -> '+ decodeURIComponent(mensaje));
                    //setTimeout(limpiarSalida(), 20000);
                }
            }
        };
    } else {
        timeOutActual *=2;
        muestra("No se ha podido establecer una conexion");
        //setTimeout(limpiarSalida(), 20000);
    }

}

function recibeJsonUrgente(mensaje) {
    var hrq = false;
    if (window.XMLHttpRequest) {// Mozilla/Chrome 
        hrq = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {// Internet Explorer
        hrq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (hrq) {
        hrq.open('POST', URL, true);
        hrq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var mensaje = encodeURIComponent(mensaje.getJSON());
        hrq.send("data="+mensaje);
        hrq.onreadystatechange = function () { // Callback
            if (hrq.readyState == 4) {
                muestra("Transferencia ajax terminada");
                if (hrq.status == 200) {
                    var params = getParam(hrq.responseText, "post");
                    var data = "";
                    if (params) {
                        // hacemos un bucle para pasar por cada indice del array de valores
                        for (var p in params) {
                            if (p == "data") {
                                data = decodeURIComponent(params[p]);
                            }
                        }
                    }
                    muestra("Se ha recibido del servidor --> "+ data);
                    muestra("Transferencia ajax terminada correctamente");
                    var tam = colaEntrada.size();
                    var tam2;
                    DesSerializar(data);
                    tam2 = colaEntrada.size();
                    if (tam < tam2){
                        confirmaMen();
                        timeOutActual = timeOut;
                        //setTimeout(limpiarSalida(), 20000);
                    }
                    //limpiarACKSalida();
                    limpiarEntrada();  
                } else{
                    timeOutActual *=2;
                    muestra('Transferencia ajax fallida');
                    muestra('Fallo al enviar el mensaje -> '+ decodeURIComponent(mensaje));
                    //setTimeout(limpiarSalida(), 20000);
                }
            }
        };
    } else {
        timeOutActual *=2;
        muestra("No se ha podido establecer una conexion");
        //setTimeout(limpiarSalida(), 20000);
    }

}